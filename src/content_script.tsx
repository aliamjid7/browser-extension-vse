import browser from "webextension-polyfill";
import { Authentication, Preferences } from "./types";
import { enableEnhancedTimetable } from "./modules/enhanced-timetable";
import { displayNonCzechLanguageWarning, enableVsePlusStatusBar } from "./modules/status";
import { enableSubmissionReminders } from "./modules/submission-reminders";
import { enableTimetablePreview } from "./modules/timetable-preview";
import { isFeatureEnabled } from "./utils";

(async function () {
  // User not logged into the system, do not inject any functionality
  if (document.querySelectorAll(".loginform-section").length > 0) {
    return;
  }

  browser.runtime.onMessage.addListener(async (message) => {
    if (message === "reload") {
      window.location.reload();
    }
  });

  const items = await browser.storage.local.get(["authentication", "preferences"]);

  const authentication: Authentication = items["authentication"] ?? {
    authenticated: false,
    username: null,
    token: null
  };

  const preferences: Preferences = items["preferences"] ?? {
    features: {}
  };

  // Preview timetable when registering courses
  if (isFeatureEnabled(preferences, "timetable-preview") && window.location.href.includes("/vyber_cviceni.pl")) {
    enableTimetablePreview();
  }


  // Display the UI for submission reminders
  if (isFeatureEnabled(preferences, "submission-reminders") && window.location.href.includes("/odevzdavarny.pl")) {
    enableSubmissionReminders(authentication);
  }

  // Replace the built-in timetable with an enhanced, interactive version
  if (isFeatureEnabled(preferences, "enhanced-timetable") && window.location.href.includes("/rozvrhy_view.pl")) {
    await enableEnhancedTimetable(authentication);
  }

  displayNonCzechLanguageWarning();
  enableVsePlusStatusBar(authentication);
})();