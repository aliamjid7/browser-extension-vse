import moment from "moment";
import { Box, Button, Divider, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Spinner, Text } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { FC } from "react";
import { cancelSubmissionReminder, createSubmissionReminder, fetchSubmissionReminders } from "../../../api";
import { SubmissionReminderDto } from "../../../api/types";
import { formatDate } from "../utils";
import { CreateReminderButtons } from "./CreateReminderButton";
import { SubmissionReminder } from "./SubmissionReminder";
import { serializeDateTime } from "../../../utils";

export interface SubmissionRemindersOverlayProps {
    token: string,
    name: string,
    course: string,
    due: Date,
    submission: number,
    onClose: () => void
};

export const SubmissionRemindersOverlay: FC<SubmissionRemindersOverlayProps> = ({ token, name, course, due, submission, onClose }: SubmissionRemindersOverlayProps) => {
    const [open, setOpen] = useState<boolean>(true);
    const [loading, setLoading] = useState<boolean>(true);
    const [reminders, setReminders] = useState<SubmissionReminderDto[]>([]);

    const close = () => {
        setOpen(false);
        onClose();
    }

    const cancelReminder = (submission: number, reminder: number) => {
        setLoading(true);

        cancelSubmissionReminder(token, submission, reminder).then(response => {
            setLoading(false);
            setReminders(response.reminders);
        });
    };

    const createReminder = (reminder: Date) => {
        setLoading(true);

        createSubmissionReminder(token, submission, name, course, serializeDateTime(moment(due)), serializeDateTime(moment(reminder)))
            .then(response => setReminders(response.reminders))
            .finally(() => setLoading(false));
    }

    useEffect(() => {
        fetchSubmissionReminders(token, submission).then(response => {
            setLoading(false);
            setReminders(response.reminders);
        });
    }, []);

    useEffect(() => {
        // Update color of the submission bell icon
        const icon = document.querySelector(`.submission-reminder-icon[data-submission='${submission}']`);
        const active = "submission-reminder-icon--active";

        if (reminders.length > 0) {
            icon?.classList.add(active);
            return;
        }

        icon?.classList.remove(active);
    }, [reminders])

    return (
        <Modal isOpen={open} onClose={close} size="xl">
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Připomenutí odezvdávárny {name}</ModalHeader>
                <ModalBody>
                    <p>
                        Odevzdávárna <b>{name}</b> z předmětu <b>{course}</b> se uzavře <b>{formatDate(due)}</b>
                    </p>
                </ModalBody>

                <Divider />

                <ModalBody>
                    {loading
                        ? <Spinner />
                        : (
                            <Box display="flex" flexFlow="column nowrap" justifyContent="center" alignItems="center">
                                {reminders.length === 0 && <Text>Pro tuto odevzdávárnu nejsou nastavena žádná upozornění</Text>}
                                {reminders.map((reminder, i) =>
                                    <SubmissionReminder key={i} reminder={new Date(reminder.reminder)} onCancel={() => cancelReminder(reminder.submission, reminder.id)} />
                                )}

                                <Divider />

                                <CreateReminderButtons due={due} createReminder={date => createReminder(date)} />
                            </Box>
                        )
                    }
                </ModalBody>

                <Divider />

                <ModalFooter>
                    <Button colorScheme='gray' mr={3} onClick={close}>
                        Zavřít nastavení
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    );
};