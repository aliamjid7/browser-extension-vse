import { ChakraProvider } from "@chakra-ui/react";
import moment, { Moment } from "moment";
import React from "react";
import { createRoot } from "react-dom/client";
import { fetchAggregateSubmissionReminders } from "../../api";
import { Authentication } from "../../types";
import { SubmissionRemindersOverlay } from "./components/SubmissionRemindersOverlay";
import { parseDateTime } from "./utils";

const deleteOverlay = (id: string) => {
    const existing = document.getElementById(id);

    // The root cannot be reused due to React internals
    if (existing != null) {
        document.body.removeChild(existing);
    }
};

const createContainer = (): HTMLDivElement => {
    const id = "submission-reminders-overlay";

    deleteOverlay(id);

    const container = document.createElement("div");

    container.id = id;
    document.body.appendChild(container);

    return container;
};

const openSubmissionRemindersOverlay = (token: string, submission: number, course: string, name: string, due: Date) => {
    const container = createContainer();
    const root = createRoot(container);

    root.render(
        <React.StrictMode>
            <ChakraProvider resetCSS={false}>
                <SubmissionRemindersOverlay token={token} submission={submission} name={name} course={course} due={due} onClose={() => deleteOverlay(container.id)} />
            </ChakraProvider>
        </React.StrictMode>
    );
}

const createGoogleCalendarExportLink = (title: string, start: Moment, end: Moment): string => {
    // The date format is: 20221018T104500Z
    // Title must be properly url encoded
    const dates = [start.format(), end.format()].map(date => date.replace(/[-:]/g, ""));

    return `https://www.google.com/calendar/render?action=TEMPLATE&text=${encodeURIComponent(title)}&dates=${dates.join("%2F")}e`
}

const createMicrosoftOutlookExportLink = (title: string, start: Moment, end: Moment): string => {
    // The date format is: 2022-10-18T12:45:00Z
    // Title must be properly url encoded
    const dates = [start.format(), end.format()]
        .map(date => date.substring(0, 16))
        .map(date => encodeURI(date));

    return `https://outlook.live.com/calendar/0/deeplink/compose?path=%2Fcalendar%2Faction%2Fcompose&rru=addevent&startdt=${dates[0]}&enddt=${dates[1]}&subject=${encodeURIComponent(title)}`;
}

enum CalendarType {
    GoogleCalendar,
    MicrosoftOutlook
}

const exportSubmissionToCalendar = (type: CalendarType, course: string, name: string, due: Date) => {
    const title = `Odevzdávárna ${name} z předmětu ${course}`;
    const date = moment(due).locale("cs");
    const link = type === CalendarType.GoogleCalendar
        ? createGoogleCalendarExportLink(title, date, date)
        : createMicrosoftOutlookExportLink(title, date, date);

    window.open(link);
}

const createHeaderEntry = (table: HTMLTableElement, authenticated: boolean) => {
    // Reminders are only displayed if the user is logged in
    const headers = (authenticated ? ["Připomenutí"] : []).concat("Export")

    headers.forEach(item => {
        const cell = document.createElement("th");
        const header = table.querySelector("thead tr.zahlavi");

        cell.classList.add("th.zahlavi");
        cell.innerText = item;

        header?.appendChild(cell);
    });
};


const createRowEntries = (table: HTMLTableElement, authentication: Authentication) => {
    table.querySelectorAll("tbody tr").forEach(row => {
        const course = row.querySelector<HTMLAnchorElement>("td > a")!.innerText;
        const name = row.querySelector<HTMLTableCellElement>("td:nth-child(3)")!.innerText;
        const due = row.querySelector<HTMLTableCellElement>("td:nth-child(6)")!.innerText;
        const link = row.querySelector<HTMLAnchorElement>("td:nth-child(12) a")!.getAttribute("href")!;
        const submission = Number(link.match(/.*odevzdavarna=(\d+).*/)![1]);
        const date = parseDateTime(due);

        if (authentication.authenticated) {
            const reminder = document.createElement("td");

            reminder.innerText = "🔔";
            reminder.classList.add("submission-reminder-icon");
            reminder.classList.add("submission-reminder-icon--loading");
            reminder.setAttribute("data-submission", submission.toString())
            reminder.addEventListener("click", () => openSubmissionRemindersOverlay(authentication.token!, submission, course, name, date));

            row.appendChild(reminder);
        }

        const calendar = document.createElement("td");

        const google = document.createElement("img");
        const outlook = document.createElement("img");

        google.setAttribute("src", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TJVIqgnYo4pChOlkQFXHUKhShQqgVWnUwufQLmhiSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxdHJSdJES/5cWWsR4cNyPd/ced+8AoV5hut01BuiGY6WTCSmbW5HEV4TQDxFRiAqzzVlZTsF3fN0jwNe7OM/yP/fn6NXyNgMCEvEMMy2HeJ14atMxOe8TR1hJ0YjPiUctuiDxI9fVJr9xLnos8MyIlUnPEUeIpWIHqx3MSpZOPEkc03SD8oVskzXOW5z1SpW17slfGM4by0tcpzmEJBawCBkSVFRRRgUO4rQapNhI037Cxz/o+WVyqeQqg5FjHhvQoXh+8D/43a1dmBhvJoUTQPeL634MA+Iu0Ki57vex6zZOgOAzcGW0/Rt1YPqT9Fpbix0BfdvAxXVbU/eAyx0g+mQqluJJQZpCoQC8n9E35YCBWyC02uyttY/TByBDXaVugINDYKRI2Ws+7+7p7O3fM63+fgAUb3KBqMhEsgAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+YKEgcaIDJlGRwAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAABXElEQVQ4y62Tu0tCcRTHP/7ygSTa4yISDTW4FAjhEkQEQeASgS0tTUH/Q1tLRGOju1tkUIOLTg1BdaFaih4QgQl27XEre9y8t0G7WP00qc70O1/O+ZzDOecHfzTHx2N04SEOKI2CtRuNq3sdQMsnIikbUE1e/alaDQBgMp+IpETVUX7RvQLg/KoOdQuivUKaYeSKlAvHtj8lA0R7BRPDHinAPDnDvJj+pElLFW/fSKZ1VjI6z68WybRO8a4shUoBnW1ORga83D1aWKZFsF1gGFbzAIBWr8DtAtOCTn9L3Uk6ZeL55SuZ3SdeDAshGq9CCujpcjMz7sZRPbNon7cyxJs6AK28YQtbORfmgXwL/Ud7hE7DIKySJ3ytQqECKJjLdtDalY/sYUAKWNq/pX3bVwJigbkjtXaIWjOnZzooAbFgdmfz22cKzQ/GAcXf4cMflHcwphvq4uy6yn/aO47JcHnYHaA9AAAAAElFTkSuQmCC");
        google.setAttribute("title", "Export do Google kalendáře");

        outlook.setAttribute("src", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAF3XpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarVdZkuUoDPznFHMEJLGI47BG9A3m+JMYcL+lqqure+x4BgOWEqUWnun//hjmH1zMgY3zUUMKweJyySXO6KhdV76eZN313C/2dJ7GzT3BGBK0sl417PVnnOyTJMro+QdBWvdEeZ5IbsvXF0G8GpmIZr9tQWkLEl4TtAXktS0bksbHLZS+2nZ2outn5sPpM+y39wjrNQ89wtyFxOIpwguAzB8byXPieioWWkno8/X0coTBIB/Z6b4SEI0J1X246ImVu0cfj5tXthzvJfJi5HC3H44b8i8TcuvhR81Od4+fx1VtXIherD9/YzQd156xi+wCTB32pm6rzQ7WFaiYqtUAWoDMAB+asuedcCu8usIVmq224K6UiMHKIEeNMg3qV1upAqLjbjiiw1xZrkGVyIkr2CNx86bBERw2UXBZL9qd8I2FLrXJVnNpU2huhKVMEEbTL757m+9+MMYMBSKrt62Ai3kaGzAmc/OJZWCExjaqvwx87tdr8ipg0E8rzxBJMGxZIoqnn5lALqIFCz3aFYMU2xYAE0G1BxgSMADWSDwFspE5EsGQCoIyoLM4LmCAvOcGkOxEArhBJEE1Pol0LWWPvBYNxpHMwISXIBHcIMpAlnMe/hOdwoeyF++898FHrz75HCS44EMIMcykmKNEZ6KPIcaoMcWsok69Bo2qmjQnToKk6VNIMWlKKWfozJCc8XXGgpwLFymueFNCiUVLKrnCfaqrvoYaq9ZUc+MmDfmjhRabttRypw5X6q77Hnrs2lPPA642xAw3/AgjDh1p5Ju1Tevb/Q3WaLPGF1NzYbxZw2iMRwTNdOInZyAMVYTAeJwUwKF5cmaVnOPJ3OTMJkZUeAZIPzlrNBkDg64T+0GHO8OL0cncX/Fmonvijf+UOTOp+yZz77x9xFqbZahejK0onEa1gujDfNfMmmexe2vNZxPfbf9O0PA8O1x6NTq00JrIyJN/DMm8qOC+VXAdBS5zTdjPWtCQry9HMSz4xl0TWctXX37emhcVtWwVWqRX97WRdO/AdBgp0nCenGd++7Lr6nWEUFhd1OKjNoRrBFm8mHaJcFbflCW/lo2UpI9bxQh1g0AV1XSEms1YsDwFoiQita6OPW26dKHK6zXiqyKKl5LmelyiDADGn+jj8t1a0xtB3ZWNJOhoD8jDNW6eISZF9rgAoLy2Pg9Y14tNPZGW9eKp4oTZLnQtx+TmZsy1m7io6ihBb5h/E7L5FeZ8rFoBWdwRJ6WOGPactwPZK1Wz3nEAcEgJGxcSXE3rs5Gnb4luh29OKm36C1GKG79Wc9PaNi4UwrJ8IgR+N/lNvtNR2s9YWYhmsChixQ23t4OM1FM9wpOCEDpOnOFjgjA7boY8l61psoGg2Fa/4yPDBb3NcQFDrdjiQeWdINqGBqOOxdo8P2+EvNILTnsR9lxkV6URlbZTxiKL94GzXfXU014Gh4xNqPjtPChBnZeJYnNpsMac9qeSsTThnHDh5FkfyvL14a2JNZ+gqup8+XIP1xZ2YOF8gtJzzRqaXkHjeIUb+YGKlB+YUBw+l+nrcHZE/+BJLCiQyxMQie4Ay1LDcSMRzKRt7AanQjHdsdtR7E4c4DB67+AYWnklzCIndYKFuj7OpKnTocE/0GDAAx0eGHO2rW9p5QwHeGU7EKB1tf34CLYniLk1l43r+M93Ek7coFHyKeS9Z8Rq2glUy1p6nSvWfBH8oWs7+k8Ko49RA7T1D6D1ZDpZFgrCtJJ/YZxHty7ay4S03an5jmba4ILyCV3HbvJsrj4qffisTtiTZBhRWj+tR+b2mRO98LLuPqtDLuSdGS2iYCc8kK9qwnFR2VUgqBwzCq1gkYe6x33XvYzS8nnJvjeUt6Oibkt/r1Vf1v7T7gqAv187l3hb0xJdGHarb3YzvzoidE3n1OKG8i8PAOY3Tgi8ch/+3dVNCY4qNyjBsSW9HSL+t9PI77YycNpM5j8h205R8qufywAAAYVpQ0NQSUNDIHByb2ZpbGUAAHicfZE9SMNAHMVfU6VSKoJ2EOmQoQqCBVERR61CESqEWqFVB5NLv6BJQ5Li4ii4Fhz8WKw6uDjr6uAqCIIfII5OToouUuL/kkKLWA+O+/Hu3uPuHSDUy0yzusYBTbfNVCIuZrKrYuAVQfQjgAhGZWYZc5KURMfxdQ8fX+9iPKvzuT9Hr5qzGOATiWeZYdrEG8TTm7bBeZ84zIqySnxOPGbSBYkfua54/Ma54LLAM8NmOjVPHCYWC22stDErmhrxFHFU1XTKFzIeq5y3OGvlKmvek78wlNNXlrlOM4IEFrEECSIUVFFCGTZitOqkWEjRfryDf8j1S+RSyFUCI8cCKtAgu37wP/jdrZWfnPCSQnGg+8VxPoaBwC7QqDnO97HjNE4A/zNwpbf8lTow80l6raVFj4C+beDiuqUpe8DlDjD4ZMim7Ep+mkI+D7yf0TdlgYFbILjm9dbcx+kDkKaukjfAwSEwUqDs9Q7v7mnv7d8zzf5+AHMFcqfh/cFWAAANeGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIgogICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICAgeG1sbnM6R0lNUD0iaHR0cDovL3d3dy5naW1wLm9yZy94bXAvIgogICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iCiAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgIHhtcE1NOkRvY3VtZW50SUQ9ImdpbXA6ZG9jaWQ6Z2ltcDphZDk2ODVkMi1mNWMwLTRhMzctODVhMC04MjgyMjljNmNmNDciCiAgIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6YTIxNDJkNzUtY2ExOC00NzQ3LWIyMGQtMDZkZjZlMjAwMGUyIgogICB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6YjFhOWIxMWEtZjMyMC00YTNkLWIzYzEtNDJhYWVhYWFkNjg0IgogICBkYzpGb3JtYXQ9ImltYWdlL3BuZyIKICAgR0lNUDpBUEk9IjIuMCIKICAgR0lNUDpQbGF0Zm9ybT0iTGludXgiCiAgIEdJTVA6VGltZVN0YW1wPSIxNjY2MDc4MTkxMzYwOTM2IgogICBHSU1QOlZlcnNpb249IjIuMTAuMzIiCiAgIHRpZmY6T3JpZW50YXRpb249IjEiCiAgIHhtcDpDcmVhdG9yVG9vbD0iR0lNUCAyLjEwIgogICB4bXA6TWV0YWRhdGFEYXRlPSIyMDIyOjEwOjE4VDA5OjI5OjUwKzAyOjAwIgogICB4bXA6TW9kaWZ5RGF0ZT0iMjAyMjoxMDoxOFQwOToyOTo1MCswMjowMCI+CiAgIDx4bXBNTTpIaXN0b3J5PgogICAgPHJkZjpTZXE+CiAgICAgPHJkZjpsaQogICAgICBzdEV2dDphY3Rpb249InNhdmVkIgogICAgICBzdEV2dDpjaGFuZ2VkPSIvIgogICAgICBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjI0MThlMWNjLTU2NjYtNGVjMy04NjY2LWIyMDlmODJiYmM1YiIKICAgICAgc3RFdnQ6c29mdHdhcmVBZ2VudD0iR2ltcCAyLjEwIChMaW51eCkiCiAgICAgIHN0RXZ0OndoZW49IjIwMjItMTAtMThUMDk6Mjk6NTErMDI6MDAiLz4KICAgIDwvcmRmOlNlcT4KICAgPC94bXBNTTpIaXN0b3J5PgogIDwvcmRmOkRlc2NyaXB0aW9uPgogPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+uuxlCQAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB+YKEgcdM/mazgUAAAHcSURBVDjLrZJNaBNRFIW/NzOZTGasNq1W08RGpYsGiiLuBMFlF67c68pF6UYX7l2IdO2+KLhURBfiD4iCrgQVSkBQsKUgojYmmulkQubNuy5S+5OUKOiBB49z7z3vvnsP/CPUTqR94XEE+H+onUkXZp5YffTsA/UXxQBlAKePzu4nDTYbK0/myeRcAAqlgF3D3kbs0cK6wNjV9y8Q8UVARNBJAnTvKq7j5MsAuEGO7JC37T1nPfEUAlNZxemix7tVm+d1jSAgycB/dAWMUMla3Js9RHUp5OKRIa7d/sTN5Zja5zWsVqOb3dEE+e0dWF0BOHc0oLoUcvbuVx6+qnHm+G5EG+JY04wSmlFCK9JE6yesr/lbBAQEEJBUQAStDaJNl++FMTSri/NbZ8Ctt03uzI3y9HyRsbzL5RvLiDaodoTl/ACguSp0Gt8REdLStLchYFKhGhqm5z8wN5Xj2ceYxVCDEazoJ2JnQXdInAS7WEI5Ts8QtXmJEb+TGq6/+VKRVPvKCGKEA8cOonWGVkfh7C2AUoOtPHHl/mvgxO+wOz6Js2cfabtN+K2BtnNgZwCoXTqs+pzoV07uuG/b8xieKBDXG0RhCG6wucYerAwyTm4kz8j4KK60Vvgf+AV34c//HB62kAAAAABJRU5ErkJggg==");
        outlook.setAttribute("title", "Export do Outlooku")

        calendar.appendChild(google);
        calendar.appendChild(outlook);

        calendar.classList.add("submission-export-icon");

        google.addEventListener("click", () => exportSubmissionToCalendar(CalendarType.GoogleCalendar, course, name, date));
        outlook.addEventListener("click", () => exportSubmissionToCalendar(CalendarType.MicrosoftOutlook, course, name, date));

        row.appendChild(calendar);
    });
};

export const enableSubmissionReminders = (authentication: Authentication) => {
    // TODO: Improve the query selector + DOM traversal?
    const icon = document.querySelector("img[src='/img.pl?unid=57']")!;
    const info = icon.parentElement!.nextElementSibling! as HTMLParagraphElement;
    const authenticated = authentication.authenticated;

    if (!authenticated) {
        info.innerHTML = "<strong>🔔 Pro aktivaci připomenutí odevzdáváren je potřeba se přihlásit po kliknutí na ikonku rozšíření VŠE+.</strong>";
    }

    const table = document.querySelector("#tmtab_1")! as HTMLTableElement;

    createHeaderEntry(table, authenticated);
    createRowEntries(table, authentication);

    if (authenticated) {
        fetchAggregateSubmissionReminders(authentication.token!).then(({ reminders }) => {
            document.querySelectorAll(".submission-reminder-icon").forEach(icon => icon.classList.remove("submission-reminder-icon--loading"));

            reminders.forEach(submission => {
                document.querySelector<HTMLTableCellElement>(`.submission-reminder-icon[data-submission='${submission}']`)
                    ?.classList.add("submission-reminder-icon--active");
            });
        });
    }
};