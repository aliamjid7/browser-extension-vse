import { Drawer, DrawerOverlay, DrawerContent, DrawerCloseButton, DrawerHeader, DrawerBody, DrawerFooter, Button, Link, Icon, Box, Text, Divider, Spinner, Spacer } from "@chakra-ui/react";
import { Moment } from "moment";
import React from "react";
import { FC } from "react";
import { FaChalkboardTeacher, FaMapMarkerAlt, FaRegCalendarAlt } from "react-icons/fa"

import { ParsedTimetableEntry, TimetableEvent } from "../types";
import { TimetableEvent as TimetableEventComponent } from "./events/TimetableEvent";

export interface TimetableEntryOverlayProps {
    date?: Moment,
    entry?: ParsedTimetableEntry,
    loading: boolean,
    events: Array<TimetableEvent>,
    onClose: () => void,
    deleteEvent: (event: TimetableEvent) => void,
    openModal: () => void,
}

export const TimetableEntryOverlay: FC<TimetableEntryOverlayProps> = ({ entry, date, loading, events, onClose, deleteEvent, openModal }: TimetableEntryOverlayProps) => {
    const open = entry !== undefined;
    const matching = events?.filter(event =>
        event.course === entry?.course.label &&
        event.date.isSame(date?.clone().add(entry.start, "minutes"), "minutes")
    ) ?? [];


    return (
        <>
            <Drawer
                isOpen={open}
                size="lg"
                placement='right'
                onClose={onClose}
            >
                {
                    entry !== undefined &&
                    date !== undefined &&
                    (
                        <>
                            <DrawerOverlay />
                            <DrawerContent>
                                <DrawerCloseButton />
                                <DrawerHeader>
                                    <Link href={entry.course.href} isExternal>
                                        {entry.course.label}
                                    </Link>
                                </DrawerHeader>

                                <DrawerBody>
                                    <Box className="timetable-overlay__info">
                                        <div className="timetable-overlay__info-item">
                                            <Icon as={FaRegCalendarAlt} marginRight="1rem" />
                                            {date.clone().add(entry.start, "minutes").format("LLL")} &ndash;
                                            {date.clone().add(entry.start + entry.duration, "minutes").format("HH:mm")}
                                        </div>
                                    </Box>

                                    <Box className="timetable-overlay__info">
                                        <Link href={entry?.teacher.href} isExternal className="timetable-overlay__info-item">
                                            <Icon as={FaChalkboardTeacher} marginRight="1rem" />
                                            <Text>{entry?.teacher.label}</Text>
                                        </Link>

                                        <Link href={entry?.room.href} isExternal className="timetable-overlay__info-item">
                                            <Icon as={FaMapMarkerAlt} marginRight="1rem" />
                                            <Text>{entry?.room.label}</Text>
                                        </Link>
                                    </Box>

                                    <Divider />

                                    {
                                        loading
                                            ? <Spinner size="xl" />
                                            : matching.map((event, i) => 
                                                <TimetableEventComponent key={i} data={event} onDelete={() => deleteEvent(event)} />
                                            )
                                    }
                                </DrawerBody>
                                <DrawerFooter justifyContent="flex-start">
                                    <Button colorScheme="blue" size="lg" onClick={() => openModal()}>Přidat událost / poznámku</Button>
                                </DrawerFooter>
                            </DrawerContent>
                        </>
                    )
                }
            </Drawer>
        </>
    );
};