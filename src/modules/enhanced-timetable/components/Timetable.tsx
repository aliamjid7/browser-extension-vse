import { Box, ChakraProvider, Spinner, Text } from "@chakra-ui/react";
import moment, { Moment } from "moment";
import React, { createRef, useEffect, useMemo, useRef, useState } from "react";
import { FC } from "react";
import { createTimetableEvent, deleteTimetableEvent, fetchTimetableEvents } from "../../../api";
import { Authentication } from "../../../types";
import { groupBy } from "../../../utils";
import { ParsedTimetable, ParsedTimetableEntry, TimetableEvent } from "../types";
import { CreateEventDialog } from "./events/CreateEventDialog";
import { TimetableEntryOverlay } from "./TimetableEntryOverlay";
import { TimetableNavigation } from "./TimetableNavigation";
import { TimetableRow } from "./TimetableRow";

export interface TimetableProps {
    data: ParsedTimetable,
    authentication: Authentication
}

export const Timetable: FC<TimetableProps> = ({ data, authentication }: TimetableProps) => {
    const days = [...new Array(5)].map((_, i) => i);
    const grouped = useMemo(() => groupBy(data.entries, item => item.day), [data]);

    // Show the next week by default during weekends
    const initialOffset = moment().locale("cs").weekday() >= 5 ? 1 : 0;
    const [offset, setOffset] = useState<number>(initialOffset);

    const [loading, setLoading] = useState<boolean>(false);
    const [events, setEvents] = useState<Array<TimetableEvent>>([]);
    const [selected, setSelected] = useState<{ entry: ParsedTimetableEntry, date: Moment } | null>(null);

    useEffect(() => {
        // Only fetch the events if the user is signed in
        if (authentication.authenticated) {
            setLoading(true);
            fetchTimetableEvents(authentication.token!)
                .then(response => {
                    const events = response.events.map(dto => ({
                        id: dto.id,
                        type: dto.type,
                        course: dto.course,
                        note: dto.note,
                        date: moment(dto.datetime)
                    }));

                    setEvents(events);
                    setLoading(false);
                });
        }

    }, []);

    const shifted = moment().locale("cs").add(offset, "weeks");
    const monday = shifted.clone().startOf("isoWeek");
    const sunday = shifted.clone().endOf("isoWeek");

    const [modalOpen, setModalOpen] = useState<boolean>(false);
    const [modalLoading, setModalLoading] = useState<boolean>(false);

    const [shiftAnimation, setShiftAnimation] = useState<string>("");

    const createEvent = (type: string, note: string) => {
        const course = selected!.entry.course.label;
        const datetime = selected!.date.clone().add(selected!.entry.start, "minutes");

        setModalLoading(true);
        createTimetableEvent(authentication.token!, course, datetime, type, note)
            .then(response => {
                const events = response.events.map(dto => ({
                    id: dto.id,
                    type: dto.type,
                    course: dto.course,
                    note: dto.note,
                    date: moment(dto.datetime)
                }));

                setEvents(events);
                setLoading(false);
                setModalLoading(false);
                setModalOpen(false);
            });
    };

    const deleteEvent = (id: number) => {
        setLoading(true);
        deleteTimetableEvent(authentication.token!, id)
            .then(response => {
                const events = response.events.map(dto => ({
                    id: dto.id,
                    type: dto.type,
                    course: dto.course,
                    note: dto.note,
                    date: moment(dto.datetime)
                }));

                setEvents(events);
                setLoading(false);
                setModalLoading(false);
                setModalOpen(false);
            });
    }

    const animationHandle = useRef<number>(0);

    const updateAnimation = (animation: string) => {
        setShiftAnimation("");
        setShiftAnimation(animation);

        if (animationHandle.current !== 0) {
            window.clearTimeout(animationHandle.current);
        }

        animationHandle.current = window.setTimeout(() => {
            setShiftAnimation("");
            animationHandle.current = 0;
        }, 300);
    };

    return (
        <ChakraProvider resetCSS={false}>
            <TimetableNavigation monday={monday} sunday={sunday}
                reset={() => { updateAnimation(offset >= initialOffset ? "previous" : "next"); setOffset(initialOffset); }}
                next={() => { updateAnimation("next"); setOffset(offset => offset + 1); }}
                previous={() => { updateAnimation("previous"); setOffset(offset => offset - 1); }}
            />
            {
                !authentication.authenticated &&
                <Box padding="1rem">
                    <strong>📅 Pro aktivaci poznámek v rozvrhu je potřeba se přihlásit po kliknutí na ikonku rozšíření VŠE+.</strong>
                </Box>
            }
            <div className="timetable-container">
                <div className={`timetable-schedule timetable-schedule--animation-${shiftAnimation || "none"}`}>
                    {days.map(day =>
                        <TimetableRow
                            key={day}
                            date={monday.clone().add(day, "days")}
                            start={data.start}
                            end={data.end}
                            entries={grouped[day] || []}
                            events={events}
                            onSelect={(entry, date) => setSelected({ entry, date })}
                        />
                    )}
                </div>
            </div>
            <Box className={`timetable__loading-indicator ${loading && "timetable__loading-indicator--active"}`}>
                <Spinner marginRight="1rem" />
                <Text fontWeight="bold">Načítání poznámek</Text>
            </Box>
            {
                authentication.authenticated &&
                <>
                    <TimetableEntryOverlay
                        entry={selected?.entry}
                        date={selected?.date}
                        events={events}
                        loading={loading}
                        onClose={() => setSelected(null)}
                        deleteEvent={(event) => deleteEvent(event.id)}
                        openModal={() => setModalOpen(true)}
                    />
                    <CreateEventDialog
                        open={modalOpen}
                        loading={modalLoading}
                        onSubmit={(type, note) => createEvent(type, note)}
                        onClose={() => {
                            setModalOpen(false);
                            setModalLoading(false);
                        }}
                    />
                </>
            }
        </ChakraProvider >
    );
};