import moment from "moment";
import { Moment } from "moment";
import React, { CSSProperties } from "react";
import { FC } from "react";
import { eventTypes } from "..";
import { ParsedTimetableEntry, TimetableEntryType, TimetableEvent } from "../types";

export interface TimetableEntryProps {
    data: ParsedTimetableEntry,
    events: Array<TimetableEvent>,
    start: Moment,
    end: Moment,
    style?: CSSProperties,
    onClick?: () => void
}

const typeClassName = (type: TimetableEntryType): string => {
    switch (type) {
        case TimetableEntryType.Lecture:
            return "timetable-entry--lecture";

        case TimetableEntryType.Seminar:
            return "timetable-entry--seminar";

        default:
            return "timetable-entry--other";
    }
}

export const TimetableEntry: FC<TimetableEntryProps> = ({ data, events, start, end, style, onClick }: TimetableEntryProps) => {
    // This date matches a day off
    const date = start.clone().startOf("day");
    const skipped = data.daysOff.some(day => moment(day).isSame(date, "days"));

    // All events maching this particular
    const matching = events.filter(event => event.course === data.course.label && event.date.isSame(start, "minutes"));
    const extraClasses: Array<string> = matching.map(event => {
        if (Object.keys(eventTypes).includes(event.type)) {
            return eventTypes[event.type];
        }

        return null;
    }).filter(it => it !== null) as Array<string>;

    const classes = [
        "timetable-entry",
        typeClassName(data.type),
        skipped && "timetable-entry--skipped",
        matching.length > 0 && "timetable-entry--has-events",
    ].concat(extraClasses.map(type => `timetable-entry--has-events-${type}`));

    return (
        <div style={style} className={classes.filter(it => it).join(" ")} onClick={() => onClick && onClick()}>
            <div className="timetable-entry__container">
                <div className="timetable-entry__time">{start.format("HH:mm")} - {end.format("HH:mm")}</div>
                <strong className="timetable-entry__course">{data.course.label}</strong>
                <div className="timetable-entry__teacher">{data.teacher.label}</div>
                <div className="timetable-entry__room">{data.room.label}</div>
            </div>

            {matching.length > 0 && <div className={`timetable-entry__events-indicator ${extraClasses.map(type => `events-indicator--${type}`).join(" ")}`}>{matching.length}</div>}
        </div>
    );
};