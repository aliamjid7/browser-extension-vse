import { Box, Button, ButtonGroup, Tooltip, Text } from "@chakra-ui/react";
import { Moment } from "moment";
import React from "react";
import { FC } from "react";

export interface TimetableNavigationProps {
    monday: Moment,
    sunday: Moment,
    previous: () => void,
    next: () => void,
    reset: () => void,
}

export const TimetableNavigation: FC<TimetableNavigationProps> = ({ monday, sunday, previous, next, reset }: TimetableNavigationProps) => {
    return (
        <Box className="timetable__navigation">
            <ButtonGroup>
                <Tooltip label="Předchozí týden">
                    <Button onClick={() => previous()} colorScheme="blue" variant="ghost">&laquo;</Button>
                </Tooltip>
                <Tooltip label="Aktuální týden">
                    <Button onClick={() => reset()} colorScheme="blue" variant="ghost">🗓️</Button>
                </Tooltip>

                <Text className="timetable__current-week">{monday.format("LL")} - {sunday.format("LL")}</Text>

                <Tooltip label="Další týden">
                    <Button onClick={() => next()} colorScheme="blue" variant="ghost">&raquo;</Button>
                </Tooltip>
            </ButtonGroup>
        </Box>
    );
};