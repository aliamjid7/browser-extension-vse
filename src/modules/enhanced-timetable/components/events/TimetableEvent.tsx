import { Box, Button, Heading, Text } from "@chakra-ui/react";
import React from "react";
import { FC } from "react";
import { eventTypes } from "../..";
import { TimetableEvent as Data } from "../../types";

export interface TimetableEventProps {
    data: Data,
    onDelete: () => void
}

export const TimetableEvent: FC<TimetableEventProps> = ({ data, onDelete }: TimetableEventProps) => {
    return (
        <Box className={`timetable-event timetable-event--${eventTypes[data.type]}`}>
            <Box className="timetable-event__text">
                <Heading size="md">{data.type}</Heading>
                {
                    data.note.length > 0
                        ? <Text>{data.note}</Text>
                        : <Text color="#a1a1aa">Bez poznámky</Text>
                }
            </Box>

            <Button colorScheme="red" onClick={onDelete}>Smazat záznam</Button>
        </Box>
    );
};