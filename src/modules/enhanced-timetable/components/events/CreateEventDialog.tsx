import { Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, ModalFooter, Button, Select, FormLabel, Divider, Textarea, Box } from "@chakra-ui/react";
import React, { FC, useEffect, useState } from "react"
import { eventTypes } from "../..";

export interface CreateEventDiagogProps {
    open: boolean,
    loading: boolean,
    onClose: () => void,
    onSubmit: (type: string, note: string) => void,
}

export const CreateEventDialog: FC<CreateEventDiagogProps> = ({ open, loading, onClose, onSubmit }: CreateEventDiagogProps) => {
    const types = Object.keys(eventTypes);

    const [type, setType] = useState<string>(types[0]);
    const [note, setNote] = useState<string>("");


    useEffect(() => {
        setType(types[0]);
        setNote("");
    }, [open])

    return (
        <Modal isOpen={open} onClose={onClose} size="lg">
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Přidat novou událost / poznámku</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <FormLabel>Typ</FormLabel>
                    <Select boxShadow="none" onChange={(event) => setType(event.target.value)} defaultValue={type}>
                        {types.map((type, i) => <option value={type} key={i}>{type}</option>)}
                    </Select>

                    <FormLabel mt={5}>Poznámka</FormLabel>
                    <Textarea className="override-insis-textarea" placeholder="Bez poznámky" value={note} onChange={(event) => setNote(event.target.value)}></Textarea>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" isLoading={loading} onClick={() => onSubmit(type, note)}>Přidat</Button>
                    <Button ml={3} onClick={onClose}>
                        Zavřít
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal >
    );
}