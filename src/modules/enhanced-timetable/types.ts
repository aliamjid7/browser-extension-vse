import { Moment } from "moment"

export enum TimetableEntryType {
    Lecture,
    Seminar,
    Other
}

export interface ParsedTimetableEntryLink {
    href: string,
    label: string
}

export interface ParsedTimetableEntry {
    room: ParsedTimetableEntryLink,
    course: ParsedTimetableEntryLink,
    teacher: ParsedTimetableEntryLink,
    day: number,  // in days, since monday
    start: number, // in minutes, since midnight,
    duration: number, // in minutes
    type: TimetableEntryType,
    daysOff: Array<Date>,
    notes: string
}

export interface ParsedTimetable {
    entries: Array<ParsedTimetableEntry>
    start: number,
    end: number
}

export interface TimetableEvent {
    id: number,
    type: string,
    course: string,
    note: string,
    date: Moment
}