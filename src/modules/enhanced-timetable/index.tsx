import React from "react";
import { createRoot } from "react-dom/client";
import { Authentication } from "../../types";
import { Timetable } from "./components/Timetable";
import { ParsedTimetable, ParsedTimetableEntry, ParsedTimetableEntryLink, TimetableEntryType } from "./types";

interface ParsedNote {
    daysOff: Array<Date>,
    additional: string
}

export const eventTypes: Record<string, string> = {
    "Průběžný test": "test",
    "Domácí úkol": "homework",
    "Prezentace": "presentation",
    "Ostatní": "other"
};

const parseNotes = (notes: HTMLTableElement): Record<number, ParsedNote> => {
    return Array.from(notes.querySelectorAll<HTMLTableRowElement>("tr"))
        .reduce((notes, row) => {
            const [label, content] = Array.from(row.querySelectorAll<HTMLTableCellElement>("td")).map(node => node.innerText);

            // Parse the note number and days off
            const number = Number(label.replace("(", "").replace(")", ""));
            const matches: Array<RegExpMatchArray> = Array.from(content.matchAll(/Volný den:\s+((?:\d+\.\s\d+\.\s+\d+(?:,\s)?)+)/g));

            // No days off?
            if (matches.length === 0) {
                return {
                    ...notes, [number]: {
                        daysOff: [],
                        additional: content,
                    }
                };
            }

            const [[full, days]] = matches;
            const daysOff = days.split(/,\s+/).map(raw => {
                const [date, month, year] = raw.split(/\.\s+/).map(Number);
                return new Date(year, month - 1, date);
            });

            return {
                ...notes,
                [number]: {
                    daysOff,
                    additional: content.replace(full, "").trim()
                }
            };
        }, {});
};

const parseTimetableStartTime = (timetable: HTMLTableElement): number => {
    // Find the start time displayed in the timetable (can be either 7:30 or 8:15)
    const header = timetable.querySelector<HTMLTableCellElement>("thead tr:first-child th:nth-child(2)")!;

    // The format is eg. 8:15-9:00
    const [start] = header.innerText.split("-").map(part => part.trim());
    const [hours, minutes] = start.split(":").map(Number);

    return hours * 60 + minutes;
};


const parseTimetable = (timetable: HTMLTableElement, notes: HTMLTableElement): ParsedTimetable => {
    const start = parseTimetableStartTime(timetable);
    const end = 19 * 60 + 30; // Check if that is the case for everyone

    const parsedNotes = parseNotes(notes);

    const rows = Array.from(timetable.querySelectorAll<HTMLTableRowElement>("tbody tr"));
    const initial: { day: number, items: Array<ParsedTimetableEntry> } = {
        day: 0,
        items: []
    };

    const result = rows.reduce((accumulator, row) => {
        const day: number = accumulator.day;

        // If the row is a separator (it has fixed height set to 1px)
        if (row.getAttribute("height") === "1") {
            // Increment the current day and continue
            return { ...accumulator, day: day + 1 };
        }

        const cells: Array<HTMLTableCellElement> = Array.from(row.querySelectorAll<HTMLTableCellElement>("td:not(.zahlavi)"));

        const initial: { time: number, items: Array<ParsedTimetableEntry> } = { time: start, items: [] };
        const result = cells.reduce((accumulator, cell) => {
            const types: Record<string, TimetableEntryType> = {
                "rozvrh-pred": TimetableEntryType.Lecture,
                "rozvrh-cvic": TimetableEntryType.Seminar,
                "rozvrh-blok": TimetableEntryType.Other
            };

            // Each segment represents 5 minutes
            const duration: number = Number(cell.getAttribute("colspan") ?? "1") * 5;
            const type = Object.keys(types)
                .map(name => cell.classList.contains(name) ? types[name] : null)
                .find(name => name !== null);

            if (type === null || type === undefined) {
                return { ...accumulator, time: accumulator.time + duration };
            }

            const references: string = cell.querySelector<HTMLSpanElement>("sup")?.innerText ?? "";
            const links: Array<ParsedTimetableEntryLink> = Array.from(cell.querySelectorAll<HTMLAnchorElement>("a"))
                .map(link => ({
                    href: (link.getAttribute("href") ?? "#"),
                    label: link.innerText.trim()
                }));

            const [room, course, teacher]: Array<ParsedTimetableEntryLink> = links.length === 3
                ? links
                // Entry with a missing course (eg. Bachelor seminar). :)))
                : [
                    links[0], 
                    {
                        label: links.reduce((text, link) => text.replace(link.label, ""), cell.innerText.replace(references, "")),
                        href: ""
                    },
                    links[1]
                ];

            const notes: Array<ParsedNote> = references
                .replace("(", "")
                .replace(")", "")
                .split(",")
                .map(Number)
                .reduce((accumulator: Array<ParsedNote>, reference) => {
                    const note: ParsedNote = parsedNotes[reference];
                    const item = {
                        daysOff: note.daysOff,
                        additional: note.additional
                    };

                    return accumulator.concat([item]);
                }, []);

            const entry: ParsedTimetableEntry = {
                room,
                course,
                teacher,
                day,
                duration,
                type,
                start: accumulator.time,
                daysOff: notes.flatMap(note => note.daysOff),
                notes: references
            };

            return { time: accumulator.time + duration, items: accumulator.items.concat([entry]) };
        }, initial);

        return { ...accumulator, items: accumulator.items.concat(result.items) };

    }, initial);


    return {
        entries: result.items,
        start: start,
        end: end
    };
};

const createContainer = (timetable: HTMLTableElement): HTMLDivElement => {
    const container = document.createElement("div");

    // Remove the legend
    // TODO: This is kinda ugly
    timetable.parentElement!.removeChild(timetable.nextSibling!);
    timetable.parentElement!.removeChild(timetable.nextSibling!);
    timetable.parentElement!.removeChild(timetable.nextSibling!);

    timetable.replaceWith(container);

    return container;
}

export const enableEnhancedTimetable = async (authentication: Authentication) => {
    // Find all tables on the page, so they can be parsed
    const [timetable, _, notes] = Array.from(document.querySelectorAll<HTMLTableElement>("table"));

    const parsed = parseTimetable(timetable, notes);
    const root = createRoot(createContainer(timetable));

    root.render(
        <Timetable data={parsed} authentication={authentication}/>
    );
};