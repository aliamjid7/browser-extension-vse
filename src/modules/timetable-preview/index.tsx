import React from "react";
import { createRoot } from "react-dom/client";
import TimetablePreview from "./components/TimetablePreview";

const createContainer = (): HTMLDivElement => {
    const container = document.createElement("div");
    document.body.appendChild(container);
    return container;
};

export const enableTimetablePreview = async () => {
    const container = createContainer();
    const root = createRoot(container)

    root.render(
        <React.StrictMode>
            <TimetablePreview />
        </React.StrictMode>
    );
}