import React, { FC } from "react";
import { AvailableTimetableEntry } from "../types";
import { computeEntryLayout } from "../utils";
import styled, { css } from "styled-components";

interface EntryProps {
    offset: number,
    width: number,
    collision: boolean,
    highlighted: boolean
}

const Entry = styled.div<EntryProps>`
    position: absolute;
    background: ${props =>
        props.collision
            ? css`transparent`
            : css`${props.highlighted ? "#e4e4e7" : "#ffffff"}`
    };
    top: 5px;
    height: 40px;
    left: ${props => props.offset}%;
    width: ${props => props.width}%;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: ${props => props.highlighted ? 1000 : 10};
    border: ${props => props.highlighted
        ? css`2px solid ${props.collision ? "#dc2626" : "#27272a"}`
        : css`2px dashed ${props.collision ? "#f87171" : "#d4d4d8"}`};
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);
    pointer-events: none;
`;

const TimetablePreviewAvailableEntry: FC<AvailableTimetableEntry> = (entry: AvailableTimetableEntry) => {
    const { offset, width } = computeEntryLayout(entry.start, entry.end);

    return <Entry offset={offset} width={width} highlighted={entry.highlighted} collision={entry.collision} />
};

export default TimetablePreviewAvailableEntry;