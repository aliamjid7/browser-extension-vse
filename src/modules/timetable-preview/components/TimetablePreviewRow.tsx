import React, { FC } from "react";
import { AvailableTimetableEntry, RegisteredTimetableEntry } from "../types";
import TimetablePreviewAvailableEntry from "./TimetablePreviewAvailableEntry";
import TimetablePreviewRegisteredEntry from "./TimetablePreviewRegisteredEntry";
import styled from "styled-components";

interface TimetablePreviewRowProps {
    registered: Array<RegisteredTimetableEntry>,
    available: Array<AvailableTimetableEntry>,
    highlighted: number
}

const Wrapper = styled.div`
    position: relative;
    height: 50px;
`

const TimetablePreviewRow: FC<TimetablePreviewRowProps> = ({ registered, available, highlighted }: TimetablePreviewRowProps) => {
    return (
        <Wrapper>
            {registered.map((entry, index) => <TimetablePreviewRegisteredEntry {...entry} key={index} />)}
            {available.map((entry, index) => <TimetablePreviewAvailableEntry {...entry} highlighted={entry.index == highlighted} key={index} />)}
        </Wrapper>
    );
};

export default TimetablePreviewRow;