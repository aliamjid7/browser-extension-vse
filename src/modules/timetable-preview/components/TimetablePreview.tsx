import React, { FC, useEffect, useState } from "react";
import { AvailableTimetableEntry, RegisteredTimetableEntry } from "../types";
import { loadCurrentTimetable, parseAvailableEntries } from "../utils";
import TimetablePreviewRow from "./TimetablePreviewRow";
import styled from "styled-components";

const Container = styled.div`
    position: fixed;
    top: 300px;
    right: 50px;
    width: 500px;
    background: white;
    padding: 20px;
    box-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1);
`;

const TimetablePreview: FC = () => {
    const [loading, setLoading] = useState<boolean>(true);
    const [registered, setRegistered] = useState<Array<RegisteredTimetableEntry>>([]);
    const [available, setAvailable] = useState<Array<AvailableTimetableEntry>>([]);
    const [highlighted, setHighlighted] = useState<number>(-1);

    useEffect(() => {
        loadCurrentTimetable()
            .then(registered => {
                setLoading(false);
                setRegistered(registered)
                setAvailable(parseAvailableEntries(registered))

                Array.from(document.querySelectorAll<HTMLElement>("[data-timetable-highlight]"))
                    .forEach(element => {
                        const index = Number(element.getAttribute("data-timetable-highlight"));

                        element.addEventListener("mouseenter", () => setHighlighted(index));
                        element.addEventListener("mouseleave", () => setHighlighted(value => value === index ? -1 : value));
                    });
            });
    }, []);

    // Group entries by weekday
    const mapped = [...new Array(5)].map((_, day) => {
        return {
            registered: registered.filter(entry => entry.day == day),
            available: available.filter(entry => entry.day == day)
        }
    });

    return (
        <Container>
            <h1>Náhled rozvrhu</h1>
            {
                loading
                    ? <h2>Načítání...</h2>
                    : mapped.map((row, index) =>
                        <TimetablePreviewRow
                            highlighted={highlighted}
                            registered={row.registered}
                            available={row.available}
                            key={index}
                        />
                    )
            }
        </Container>
    );
};

export default TimetablePreview;