import browser, { runtime } from "webextension-polyfill";
import { ChakraProvider } from "@chakra-ui/react";
import React, { FC, useEffect, useState, createContext } from "react";
import { createRoot } from "react-dom/client";
import styled from "styled-components";
import { Authentication } from "./types";
import Login from "./components/Login";
import User from "./components/User";
import Preferences from "./components/Preferences";

export interface PopupContextValue {
  authentication: Authentication | null,
  setAuthentication: (authentication: Authentication) => void
}

export const PopupContext = createContext<PopupContextValue>({
  authentication: null,
  setAuthentication: () => { }
});

const reload = async () => {
  const tabs = await browser.tabs.query({ url: "https://insis.vse.cz/*" });
  tabs.forEach(tab => browser.tabs.sendMessage(tab.id!, "reload"));
}

const Wrapper = styled.div`
  min-width: 400px;
  min-height: 200px;
  padding: 10px;
`;

const Popup: FC = () => {
  const [authentication, setAuthentication] = useState<Authentication | null>(null);
  const setAuthenticationPersisting = (authentication: Authentication | null) => {
    setAuthentication(authentication);
    browser.storage.local.set({ authentication });
  };

  useEffect(() => {
    browser.storage.local.get("authentication").then(items => {
      const authentication: Authentication = items["authentication"] ?? {
        authenticated: false,
        username: null,
        token: null
      };

      setAuthenticationPersisting(authentication);
    });
  }, []);

  return (
    <ChakraProvider>
      <PopupContext.Provider value={{
        authentication: authentication,
        setAuthentication: setAuthenticationPersisting
      }}>
        <Wrapper>
          {
            authentication !== null && authentication.authenticated
              ? <User username={authentication.username!} />
              : <Login />
          }

          <hr/>

          <Preferences onUpdate={() => reload()} />
        </Wrapper>
      </PopupContext.Provider>
    </ChakraProvider>
  );
};

const root = createRoot(document.getElementById("root")!)

root.render(
  <React.StrictMode>
    <Popup />
  </React.StrictMode>
);