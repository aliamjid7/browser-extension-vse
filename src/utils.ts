// I spent like 30 minutes on this simple function :)))

import { Moment } from "moment";
import { Preferences } from "./types";

type Grouping<K extends string | number | symbol, V> = Record<K, Array<V>>

export const groupBy = <V, K extends string | number | symbol>(array: Array<V>, selector: (item: V) => K): Grouping<K, V> => {
    return array.reduce((grouped: Grouping<K, V>, item: V): Grouping<K, V> => {
        const key = selector(item);
        const items = (grouped[key] || []) as Array<V>;

        return { ...grouped, [key]: items.concat([item]) };
    }, {} as Grouping<K, V>);
};

export const collides = (first: { start: number, end: number }, second: { start: number, end: number }): boolean => {
    return (first.start < second.end && first.end >= second.end) || (first.start < second.start && first.end >= second.start);
};

export const isFeatureEnabled = (preferences: Preferences, feature: string): boolean => {
    // Fallback to all features enabled by default for discoverability
    if (!preferences || !Object.keys(preferences.features).includes(feature)) {
        return true;
    }

    return preferences.features[feature];
}

export const serializeDateTime = (moment: Moment): string => {
    return moment.format().substring(0, 16);
};