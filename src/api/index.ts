import { Base64 } from "js-base64";
import { Moment } from "moment";
import { serializeDateTime } from "../utils";
import { AggregateRemindersResponse, CreateAccountRequest, CreateAccountResponse, DetailedRemindersResponse, TimetableEventsResponse, VerifyAccountRequest, VerifyAccountResponse } from "./types";

const domain = "vse.vrba.dev";
const base = `https://${domain}/api/v1`;

const headers = (token: string | null = null): { [key: string]: string } => {
    const authorization: { [key: string]: string } = token !== null
        ? { "Authorization": `Bearer ${token}` }
        : {};

    return {
        ...authorization,
        "Accept": "application/json",
        "Content-Type": "application/json",
    };
};

export const verifyToken = async (token: string): Promise<boolean> => {
    return await fetch(`${base}/account/info`, {
        method: "get",
        headers: headers(token)
    }).then(response => response.ok);
};

export const createAccount = async (username: string): Promise<CreateAccountResponse> => {
    const request: CreateAccountRequest = { username };
    const response: CreateAccountResponse = await fetch(`${base}/account/create`, {
        method: "post",
        body: JSON.stringify(request),
        headers: headers()
    }).then(raw => raw.json());

    return response;
};

export const verifyAccount = async (username: string, code: string): Promise<VerifyAccountResponse> => {
    const request: VerifyAccountRequest = { username, code };
    const response: VerifyAccountResponse = await fetch(`${base}/account/verify`, {
        method: "post",
        body: JSON.stringify(request),
        headers: headers()
    }).then(raw => raw.json());

    return response;
};

export const fetchAggregateSubmissionReminders = async (token: string): Promise<AggregateRemindersResponse> => {
    return await fetch(`${base}/submission-reminders`, {
        method: "get",
        headers: headers(token)
    }).then(response => response.json());
};

export const fetchSubmissionReminders = async (token: string, submission: number): Promise<DetailedRemindersResponse> => {
    return await fetch(`${base}/submission-reminders/${submission}`, {
        method: "get",
        headers: headers(token)
    }).then(response => response.json());
}

export const createSubmissionReminder = async (token: string, submission: number, name: string, course: string, due: string, reminder: string): Promise<DetailedRemindersResponse> => {
    return await fetch(`${base}/submission-reminders/create`, {
        method: "post",
        body: JSON.stringify({ submission, name, course, due, reminder }),
        headers: headers(token)
    }).then(response => response.json());
}

export const cancelSubmissionReminder = async (token: string, submission: number, reminder: number): Promise<DetailedRemindersResponse> => {
    return await fetch(`${base}/submission-reminders/${submission}/${reminder}/cancel`, {
        method: "post",
        headers: headers(token)
    }).then(response => response.json());
}

export const fetchTimetableEvents = async (token: string): Promise<TimetableEventsResponse> => {
    return await fetch(`${base}/timetable-events`, {
        method: "get",
        headers: headers(token)
    }).then(response => response.json());
};

export const createTimetableEvent = async (token: string, course: string, datetime: Moment, type: string, note: string): Promise<TimetableEventsResponse> => {
    return await fetch(`${base}/timetable-events/create`, {
        method: "post",
        headers: headers(token),
        body: JSON.stringify({
            course,
            type,
            note,
            datetime: serializeDateTime(datetime)
        })
    }).then(response => response.json());
};

export const deleteTimetableEvent = async (token: string, id: number): Promise<TimetableEventsResponse> => {
    return await fetch(`${base}/timetable-events/${id}/delete`, {
        method: "post",
        headers: headers(token),
    }).then(response => response.json());
};
