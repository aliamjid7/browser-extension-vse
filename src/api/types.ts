export interface CreateAccountRequest {
    username: string
}

export interface VerifyAccountRequest {
    username: string,
    code: string
}

export interface AccountResponse {
    username: string,
    email: string,
    verified: boolean,
    discord: number | null
}

export interface CreateAccountResponse {
    username: string,
    message: string
}

export interface VerifyAccountResponse {
    username: string,
    token: string
}

export interface AggregateRemindersResponse {
    reminders: Array<number>
}

export interface SubmissionReminderDto {
    id: number,
    submission: number,
    name: string,
    course: string,
    due: string,
    reminder: string
}

export interface DetailedRemindersResponse {
    reminders: Array<SubmissionReminderDto>
}

export interface TimetableEventDto {
    id: number,
    course: string,
    datetime: string,
    type: string,
    note: string
}

export interface TimetableEventsResponse {
    events: Array<TimetableEventDto>
}