import browser from "webextension-polyfill";
import React, { useEffect, useState } from "react";
import { FC } from "react";
import { FormControl, FormLabel, Switch } from "@chakra-ui/react";
import { isFeatureEnabled } from "../utils";
import { Preferences as PreferencesInterface } from "../types";

export interface PreferencesProps {
    onUpdate: () => void
}

const Preferences: FC<PreferencesProps> = ({ onUpdate }: PreferencesProps) => {
    const [preferences, setPreferences] = useState<PreferencesInterface>({ features: {} });

    const names: Record<string, string> = {
        "enhanced-timetable": "Vylepšený rozvrh",
        "submission-reminders": "Připomenutí odevzdáváren",
        "timetable-preview": "Náhled rozvrhu při registracích"
    }

    useEffect(() => {
        browser.storage.local.get("preferences").then(items => { setPreferences(items["preferences"] || { features: {} }) })
    }, []);

    const setFeature = (feature: string, value: boolean) => {
        setPreferences(preferences => {
            const updated = { ...preferences.features, [feature]: value };

            browser.storage.local.set({ "preferences": { features: updated } });

            return { features: updated };
        });

        onUpdate();
    }

    return (
        <>
            {Object.keys(names).map((feature, i) => {
                const enabled = isFeatureEnabled(preferences, feature);

                return (
                    <FormControl display="flex" alignItems="center" mb="1" mt="1" key={i}>
                        <Switch id={feature} isChecked={enabled} onChange={(event) => setFeature(feature, event.target.checked)} />
                        <FormLabel htmlFor={feature} mb="0" ml="1">
                            {names[feature]}
                        </FormLabel>
                    </FormControl>
                );
            })}
        </>
    );
};

export default Preferences;